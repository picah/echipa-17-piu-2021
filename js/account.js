window.onload = (
    function(){
        let savedLocalStorege = localStorage.getItem("savedFace");
        if (savedLocalStorege) {
            savedFace = JSON.parse(savedLocalStorege);
            console.log(localStorage.getItem("notCreatedAvatarFlag"));
            console.log(document.getElementById("face").src);
            if(document.getElementById("face").src == "data:," && 
                document.getElementById("eye").src == "data:," && 
                document.getElementById("nose").src == "data:," && 
                document.getElementById("mouth").src == "data:," && 
                document.getElementById("brow").src == "data:," && 
                document.getElementById("hair").src == "data:," && 
                localStorage.getItem("notCreatedAvatarFlag") == 1) {
                    localStorage.setItem("notCreatedAvatarFlag", "firstAvatarWasCreated");
                }

            if(savedFace.face) {
                document.getElementById("face").src = savedFace.face;
            }
            if(savedFace.eye) {
                document.getElementById("eye").src = savedFace.eye;
            }
            if(savedFace.nose) {
                document.getElementById("nose").src = savedFace.nose;
            }
            if(savedFace.mouth) {
                document.getElementById("mouth").src = savedFace.mouth;
            }
            if(savedFace.brow) {
                document.getElementById("brow").src = savedFace.brow;
            }
            if(savedFace.hair) {
                document.getElementById("hair").src = savedFace.hair;
            }
        }
    }
);