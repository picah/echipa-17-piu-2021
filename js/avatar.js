var savedFace = {
    face: '',
    eye: '',
    nose: '',
    mouth: '',
    brow: '',
    hair: ''
};

window.onload = (
    function(){
        let savedLocalStorege = localStorage.getItem("savedFace");
        if (savedLocalStorege) {
            savedFace = JSON.parse(savedLocalStorege);
            if(savedFace.face) {
                document.getElementById("face").src = savedFace.face;
            }
            if(savedFace.eye) {
                document.getElementById("eye").src = savedFace.eye;
            }
            if(savedFace.nose) {
                document.getElementById("nose").src = savedFace.nose;
            }
            if(savedFace.mouth) {
                document.getElementById("mouth").src = savedFace.mouth;
            }
            if(savedFace.brow) {
                document.getElementById("brow").src = savedFace.brow;
            }
            if(savedFace.hair) {
                document.getElementById("hair").src = savedFace.hair;
            }
        } else {
            localStorage.setItem("notCreatedAvatarFlag", 1);
        }
        
        loadColor1();
        loadColor1Eye();
        loadColor1Brow();
        loadNose();
        loadMouth();
        loadColor1Hair();
    }
);

function loadColor1() {
    var elem = document.getElementById("facetypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('facetypes');

    for(let i = 1; i <= 48; i++) {
        html = '';
        
        html += `<img src="images/face/face_color1/file` + i + `.png" class="mr-6" 
                    onclick='changeSavedFace(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor2() {
    var elem = document.getElementById("facetypes");
    elem.innerHTML = '';


    var html = '';
    var faceTypes = document.getElementById('facetypes');

    for(let i = 1; i <= 46; i++) {
        html = '';
        
        html += `<img src="images/face/face_color2/file` + i + `.png" class="mr-6"
                    onclick='changeSavedFace(this.src);'>
            </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor3() {
    var elem = document.getElementById("facetypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('facetypes');

    for(let i = 1; i <= 48; i++) {
        html = '';
        
        html += `<img style='height: 160px; width:160px;' src="images/face/face_color3/file` + i + `.png"
                    onclick='changeSavedFace(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor4() {
    var elem = document.getElementById("facetypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('facetypes');

    for(let i = 1; i <= 48; i++) {
        html = '';
        
        html += `<img src="images/face/face_color4/file` + i + `.png" class="mr-6"
                    onclick='changeSavedFace(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function changeSavedFace(src) {
    document.getElementById("face").src= src;

    savedFace.face = src;
    console.log(savedFace.face);

    localStorage.setItem("savedFace", JSON.stringify(savedFace));

    console.log(JSON.parse(localStorage.getItem("savedFace")));
    console.log(savedFace);

}


function loadColor1Eye() {
    var elem = document.getElementById("eyetypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('eyetypes');

    for(let i = 1; i <= 50; i++) {
        html = '';
        
        html += `<img src="images/eyes/eyes1/file` + i + `.png"
                    onclick='changeSavedEye(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor2Eye() {
    var elem = document.getElementById("eyetypes");
    elem.innerHTML = '';


    var html = '';
    var faceTypes = document.getElementById('eyetypes');

    for(let i = 1; i <= 38; i++) {
        html = '';
        
        html += `<img src="images/eyes/eyes2/file` + i + `.png"
                    onclick='changeSavedEye(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor3Eye() {
    var elem = document.getElementById("eyetypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('eyetypes');

    for(let i = 1; i <= 58; i++) {
        html = '';
        
        html += `<img src="images/eyes/eyes3/file` + i + `.png"
                    onclick='changeSavedEye(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor4Eye() {
    var elem = document.getElementById("eyetypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('eyetypes');

    for(let i = 1; i <= 73; i++) {
        html = '';
        
        html += `<img src="images/eyes/eyes4/file` + i + `.png"
                    onclick='changeSavedEye(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function changeSavedEye(src) {
    document.getElementById("eye").src = src;

    savedFace.eye = src;
    console.log(savedFace.eye);

    localStorage.setItem("savedFace", JSON.stringify(savedFace));

    console.log(JSON.parse(localStorage.getItem("savedFace")));
    console.log(savedFace);
}

function loadColor1Brow() {
    var elem = document.getElementById("browtypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('browtypes');

    for(let i = 1; i <= 60; i++) {
        html = '';
        
        html += `<img src="images/brows/brows1/file` + i + `.png" class="mr-6"
                    onclick='changeSavedBrow(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor2Brow() {
    var elem = document.getElementById("browtypes");
    elem.innerHTML = '';


    var html = '';
    var faceTypes = document.getElementById('browtypes');

    for(let i = 1; i <= 60; i++) {
        html = '';
        
        html += `<img src="images/brows/brows2/file` + i + `.png" class="mr-6"
                    onclick='changeSavedBrow(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor3Brow() {
    var elem = document.getElementById("browtypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('browtypes');

    for(let i = 1; i <= 60; i++) {
        html = '';
        
        html += `<img src="images/brows/brows3/file` + i + `.png" class="mr-6"
                    onclick='changeSavedBrow(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function changeSavedBrow(src) {
    document.getElementById("brow").src = src;

    savedFace.brow = src;
    //console.log(savedFace.brow);

    localStorage.setItem("savedFace", JSON.stringify(savedFace));

    console.log(JSON.parse(localStorage.getItem("savedFace")));
    console.log(savedFace);
}

function loadNose() {
    var elem = document.getElementById("nosetypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('nosetypes');

    for(let i = 1; i <= 60; i++) {
        html = '';
        
        html += `<img src="images/nose/file` + i + `.png"
                    onclick='changeSavedNose(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function changeSavedNose(src) {
    document.getElementById("nose").src = src;

    savedFace.nose = src;
    //console.log(savedFace.brow);

    localStorage.setItem("savedFace", JSON.stringify(savedFace));

    console.log(JSON.parse(localStorage.getItem("savedFace")));
    console.log(savedFace);
}

function loadMouth() {
    var elem = document.getElementById("mouthtypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('mouthtypes');

    for(let i = 1; i <= 61; i++) {
        html = '';
        
        html += `<img src="images/mouth/file` + i + `.png"
                    onclick='changeSavedMouth(this.src);'>
                </img>`;

        // //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function changeSavedMouth(src) {
    document.getElementById("mouth").src = src;

    savedFace.mouth = src;
    //console.log(savedFace.brow);

    localStorage.setItem("savedFace", JSON.stringify(savedFace));

    console.log(JSON.parse(localStorage.getItem("savedFace")));
    console.log(savedFace);
}

function loadColor1Hair() {
    var elem = document.getElementById("hairtypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('hairtypes');

    for(let i = 1; i <= 120; i++) {
        html = '';
        
        html += `<img src="images/hairs/hair1/file` + i + `.png"
                    onclick='changeSavedHair(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor2Hair() {
    var elem = document.getElementById("hairtypes");
    elem.innerHTML = '';


    var html = '';
    var faceTypes = document.getElementById('hairtypes');

    for(let i = 1; i <= 121; i++) {
        html = '';
        
        html += `<img src="images/hairs/hair2/file` + i + `.png"
                    onclick='changeSavedHair(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor3Hair() {
    var elem = document.getElementById("hairtypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('hairtypes');

    for(let i = 1; i <= 120; i++) {
        html = '';
        
        html += `<img src="images/hairs/hair3/file` + i + `.png"
                    onclick='changeSavedHair(this.src);'>
                 </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function loadColor4Hair() {
    var elem = document.getElementById("hairtypes");
    elem.innerHTML = '';

    var html = '';
    var faceTypes = document.getElementById('hairtypes');

    for(let i = 1; i <= 120; i++) {
        html = '';
        
        html += `<img src="images/hairs/hair4/file` + i + `.png"
                    onclick='changeSavedHair(this.src);'>
                </img>`;

        //console.log(html);
        faceTypes.innerHTML += html;
    }
}

function changeSavedHair(src) {
    document.getElementById("hair").src = src;

    savedFace.hair = src;
    //console.log(savedFace.brow);

    localStorage.setItem("savedFace", JSON.stringify(savedFace));

    console.log(JSON.parse(localStorage.getItem("savedFace")));
    console.log(savedFace);
}

