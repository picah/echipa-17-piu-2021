window.onload = (
    function(){
        // SETTING UP THE NEW BADGES
        var flag = localStorage.getItem("notCreatedAvatarFlag")
        if(flag == "firstAvatarWasCreated") {
            var html = ` <div style='position:relative;' id="firstAvatarNewBadge" class="trigger_popup_fricc">
            <img class="h-40 m-2 drop-shadow-lg" src="images/badges/file5.png">
            <img class="h-10 m-2 drop-shadow-lg" src="images/badges/newicon.png" style='position:absolute; top:10px; left:0px;'>
            </div>`;
            
            var accountIcon = document.getElementById('newBadgesList');
            accountIcon.innerHTML += html;
        }

        // BADGE FOR THE FIRST AVATAR
        let savedLocalStorege = localStorage.getItem("savedFace");
        if (savedLocalStorege) {
            var html = '';

            if(document.getElementById("firstAvatarE")) {
                $(".firstAvatarE").remove();
            }

            if(document.getElementById("firstAvatarA")) {
                $(".firstAvatarA").remove();
            }
            
            html += `<img class="h-40 m-2 drop-shadow-lg" src="images/badges/file5.png" id="firstAvatarE">`;
            document.getElementById("earnedBadges").innerHTML += html;
        } else {
            var html = '';

            if(document.getElementById("firstAvatarE")) {
                $(".firstAvatarE").remove();
            }

            if(document.getElementById("firstAvatarA")) {
                $(".firstAvatarA").remove();
            }
        
            html += `<img class="h-40 m-2 drop-shadow-lg" src="images/badges/file5.png" id="firstAvatarA">`;
            document.getElementById("availableBadges").innerHTML += html;
        }

        // BADGE FOR CHRISTMAS AND EASTER
        if(new Date().getMonth() <= 4 || new Date().getMonth() >= 12) {
            var html = '';

            if(document.getElementById("christmasAvatarE")) {
                $(".christmasAvatarE").remove();
            }

            if(document.getElementById("christmasAvatarA")) {
                $(".christmasAvatarA").remove();
            }
            
            html += `<img class="h-40 m-2 drop-shadow-lg" src="images/badges/file7.png">`;
            document.getElementById("earnedBadges").innerHTML += html;


            var html = '';

            if(document.getElementById("easterAvatarE")) {
                $(".easterAvatarE").remove();
            }

            if(document.getElementById("easterAvatarA")) {
                $(".easterAvatarA").remove();
            }
            
            html += `<img class="h-40 m-2 drop-shadow-lg" src="/images/badges/file6.png">`;
            document.getElementById("availableBadges").innerHTML += html;
        } else {
                var html = '';
    
                if(document.getElementById("christmasAvatarE")) {
                    $(".christmasAvatarE").remove();
                }
    
                if(document.getElementById("christmasAvatarA")) {
                    $(".christmasAvatarA").remove();
                }
                
                html += `<img class="h-40 m-2 drop-shadow-lg" src="images/badges/file7.png">`;
                document.getElementById("availableBadges").innerHTML += html;
    
    
                var html = '';
    
                if(document.getElementById("easterAvatarE")) {
                    $(".easterAvatarE").remove();
                }
    
                if(document.getElementById("easterAvatarA")) {
                    $(".easterAvatarA").remove();
                }
                
                html += `<img class="h-40 m-2 drop-shadow-lg" src="/images/badges/file6.png">`;
                document.getElementById("earnedBadges").innerHTML += html;
        }

        // BADGE FOR THE FIRST 100 MONEY
        const localStorageIncome = JSON.parse(localStorage.getItem("income"));
        
        if (parseInt(localStorageIncome, 10) >= 100) {
            var html = '';

            if(document.getElementById("first100AvatarE")) {
                $(".first100AvatarE").remove();
            }

            if(document.getElementById("first100AvatarA")) {
                $(".first100AvatarA").remove();
            }
            
            html += `<img class="h-40 m-2 drop-shadow-lg" src="images/badges/file3.png" id="first100AvatarE">`;
            document.getElementById("earnedBadges").innerHTML += html;
        } else {
            var html = '';

            if(document.getElementById("first100AvatarE")) {
                $(".first100AvatarE").remove();
            }

            if(document.getElementById("first100AvatarA")) {
                $(".first100AvatarA").remove();
            }
        
            html += `<img class="h-40 m-2 drop-shadow-lg" src="images/badges/file3.png" id="first100AvatarA">`;
            document.getElementById("availableBadges").innerHTML += html;
        }

        // BADGE FOR THE FIRST 1000 MONEY
        if (parseInt(localStorageIncome, 10) >= 1000) {
            var html = '';

            if(document.getElementById("first1000AvatarE")) {
                $(".first1000AvatarE").remove();
            }

            if(document.getElementById("first100AvatarA")) {
                $(".first1000AvatarA").remove();
            }
            
            html += `<img class="h-40 m-2 drop-shadow-lg" src="images/badges/file2.png" id="first1000AvatarE">`;
            document.getElementById("earnedBadges").innerHTML += html;
        } else {
            var html = '';

            if(document.getElementById("first1000AvatarE")) {
                $(".first1000AvatarE").remove();
            }

            if(document.getElementById("first100AvatarA")) {
                $(".first1000AvatarA").remove();
            }
        
            html += `<img class="h-40 m-2 drop-shadow-lg" src="images/badges/file2.png" id="first1000AvatarA">`;
            document.getElementById("availableBadges").innerHTML += html;
        }

        // BADGE FOR THE FIRST 10000 MONEY
        if (parseInt(localStorageIncome, 10) >= 10000) {
            var html = '';

            if(document.getElementById("first10000AvatarE")) {
                $(".first10000AvatarE").remove();
            }

            if(document.getElementById("first1000AvatarA")) {
                $(".first10000AvatarA").remove();
            }
            
            html += `<img class="h-40 m-2 drop-shadow-lg" src="images/badges/file4.png" id="first10000AvatarE">`;
            document.getElementById("earnedBadges").innerHTML += html;
        } else {
            var html = '';

            if(document.getElementById("first10000AvatarE")) {
                $(".first10000AvatarE").remove();
            }

            if(document.getElementById("first1000AvatarA")) {
                $(".first10000AvatarA").remove();
            }
        
            html += `<img class="h-40 m-2 drop-shadow-lg" src="images/badges/file4.png" id="first10000AvatarA">`;
            document.getElementById("availableBadges").innerHTML += html;
        }

        // TO OPEN AND CLOSE THE POP UP
        $(".trigger_popup_fricc").click(function(){
            $('.hover_bkgr_fricc').show();
         });
         $('.hover_bkgr_fricc').click(function(){
            if(document.getElementById("firstAvatarNewBadge")) {
                document.getElementById("firstAvatarNewBadge").remove();
            }
            localStorage.setItem("notCreatedAvatarFlag", "2");

             $('.hover_bkgr_fricc').hide();
         });
         $('.popupCloseButton').click(function(){
            if(document.getElementById("firstAvatarNewBadge")) {
                document.getElementById("firstAvatarNewBadge").remove();
            }
            localStorage.setItem("notCreatedAvatarFlag", "2");
            console.log("sunt aici");

             $('.hover_bkgr_fricc').hide();
         });
    }
);