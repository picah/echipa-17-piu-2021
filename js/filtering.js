data = [
    { type: 'expenses', sum: 21, id: 3221, description: 'taxi'},
    { type: 'expenses', sum: 42, id: 21212, description: 'monday lunch' },
    { type: 'income', sum: 31332, id: 20055, description: 'salary'},
    { type: 'expenses', sum: 21212, id: 20053, description: 'for vacation'},
    { type: 'income', sum: 32338, id: 45652, description: 'rents DECEM' },
    { type: 'expenses', sum: 212, id: 22121, description: 'friday sushi'},
    { type: 'expenses', sum: 12, id: 2053, description: 'food'},
    { type: 'income', sum: 322, id: 39232, description: 'little business' }
];

window.onload = (
    function(){
        var tablebody = document.getElementById('tableBody');

        for(let d of data) {
            var html = `<tr>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.id + `</td>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.sum + `</td>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.type + `</td>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.description + `</td>
                        </tr>`;

            tablebody.innerHTML += html;
        }
    }
);

function sortByAsc() {
    data.sort(function(a, b) {
        return parseFloat(a.sum) - parseFloat(b.sum);
    });

    var tablebody = document.getElementById('tableBody');
    $("#tableBody tr").remove();

    for(let d of data) {
        var html = `<tr>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.id + `</td>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.sum + `</td>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.type + `</td>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.description + `</td>
                        </tr>`;

        tablebody.innerHTML += html;
    }
}

function sortByDesc() {
    data.sort(function(a, b) {
        return parseFloat(b.sum) - parseFloat(a.sum);
    });

    var tablebody = document.getElementById('tableBody');
    $("#tableBody tr").remove();

    for(let d of data) {
        var html = `<tr>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.id + `</td>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.sum + `</td>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.type + `</td>
                            <td class="pl-4 py-4 text-xl font-light border-2 border-sky-800">` + d.description + `</td>
                        </tr>`;

        tablebody.innerHTML += html;
    }
}