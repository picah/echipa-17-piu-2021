var reminders = [
    {id: 1, name:'Reminder1'},
    {id: 2, name:'Reminder2'},
    {id: 5, name:'Reminder3'},
    {id: 3, name:'Reminder4'},
    {id: 4, name:'Reminder5'},
    {id: 6, name:'Reminder6'},
    {id: 7, name:'Reminder7'},
    {id: 8, name:'Reminder8'}
]

var incomeValue = 32762;
//var incomeValue = 1;
var expensiseValue = 9239;
var balanceValue = 23523;

var incomeValueFamily = 100000;
//var incomeValue = 1;
var expensiseValueFamily = 9239;
var balanceValueFamily = 100761;

window.onload = (
    function(){
        document.getElementById("income").value = incomeValue;
        document.getElementById("expensise").value = expensiseValue;
        document.getElementById("balance").value = balanceValue;

        settingBudgetOnLocalStorage();
        populateRemindersList();

        var flag = localStorage.getItem("notCreatedAvatarFlag")
        if(flag == "firstAvatarWasCreated") {
            var html = `<svg xmlns="http://www.w3.org/2000/svg" class="drop-shadow-xl text-red-600" fill="none" viewBox="0 0 56 56" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z" />
                        </svg>`; 
            
            var accountIcon = document.getElementById('svg');
            accountIcon.innerHTML += html;
        }
    }
);

function settingBudgetOnLocalStorage() {
    var income = document.getElementById("income").value;
    var expensise = document.getElementById("expensise").value;
    var balance = document.getElementById("balance").value;

    localStorage.setItem("income", income);
    localStorage.setItem("expensise", expensise);
    localStorage.setItem("balance", balance);

    localStorage.setItem("incomeFamily", JSON.stringify(incomeValueFamily));
    localStorage.setItem("expensiseFamily", JSON.stringify(expensiseValueFamily));
    localStorage.setItem("balanceFamily", JSON.stringify(balanceValueFamily));
}

function populateRemindersList() {
    var html = '';
    var remindersList = document.getElementById('reminders');

    for(let reminder of reminders) {
        html = '';
        
        html += `<li id="` + reminder.id + `" class="py-6 px-4 my-6 drop-shadow-md rounded-2xl bg-slate-50/75 dark-blue-color">  
                    <div class="flex justify-between">` + reminder.name;
            html += ` <div onclick='deleteReminder($(this).closest("li").attr("id"));' >
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor"> 
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" /> 
                        </svg> 
                      </div>
                    </div> 
                </li>`;

        console.log(html);
        remindersList.innerHTML += html;
    }
}

function deleteReminder(id) {
    console.log(id);

    var el = document.getElementById(id);
    el.remove();
}

function showPersonalAccount() {
    document.getElementById("income").value = localStorage.getItem("income");
    document.getElementById("expensise").value = localStorage.getItem("expensise");
    document.getElementById("balance").value = localStorage.getItem("balance");
}

function showFamilyAccount() {
    document.getElementById("income").value = localStorage.getItem("incomeFamily");
    document.getElementById("expensise").value = localStorage.getItem("expensiseFamily");
    document.getElementById("balance").value = localStorage.getItem("balanceFamily");
}
function unificationPage()
{
    window.location="unification.html";
}