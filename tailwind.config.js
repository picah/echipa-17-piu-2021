module.exports = {
  content: ["./js/*.js", "./home.html", "./account.html", "./avatar.html", "./badge.html", "./tools.html", "./filtering.html", "./settings.html"],
  darkMode: "class",
  theme: {
    extend: {},
  },
  plugins: [
    require('daisyui'),
  ],
  // Modify DaisyUI's "emerald" theme colors
  // config (optional)
  daisyui: {
    themes: [
      'cupcake'
    ],
  },
}
